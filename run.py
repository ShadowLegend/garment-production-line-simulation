
import os
import json
import subprocess
from random import randint
from pathlib import Path, PureWindowsPath

SIMULATION_RUN_COUNT = 5
TEMP_SIMULATION_RUN_GEN_PATH = Path('./tmp')

if not os.path.exists(TEMP_SIMULATION_RUN_GEN_PATH):
    os.makedirs(TEMP_SIMULATION_RUN_GEN_PATH)

with open('run_config.json', 'r') as config_file:
    json_config = json.load(config_file)

    for config in json_config['configs']:
        for idx in range(SIMULATION_RUN_COUNT):
            filename = config['filename']
            filename_without_extension = config['filename'].split('.')[0]
            run_config_path = Path(f'{TEMP_SIMULATION_RUN_GEN_PATH}/run-{filename_without_extension}-{idx}.cfg')

            with open(run_config_path, 'a+') as run_cfg:
                run_cfg.write(f'Include \'{os.getcwd()}\\{filename}\'\n')
                run_cfg.write('Simulation RealTime { FALSE }\n')
                run_cfg.write('Simulation ExitAtStop { TRUE }\n')
                run_cfg.write('Simulation PrintReport { TRUE }\n')
                run_cfg.write('\n'.join([f'{dist} RandomSeed {{ {randint(1, 99999)} }}' for dist in config['distributions']]))

            subprocess.run(['jaamsim', str(PureWindowsPath(run_config_path.absolute())), '-b'])
